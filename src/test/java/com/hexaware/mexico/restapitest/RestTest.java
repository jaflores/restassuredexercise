package com.hexaware.mexico.restapitest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.*;

public class RestTest {
	Map<String, String> paramsMap = new HashMap<>();
	Map<String, String> headersMap = new HashMap<>();

	@BeforeClass
	public static void classSetup() {
		try {
			RestAssured.baseURI = "https://reqres.in";
			RestAssured.port = 443;
			RestAssured.basePath = "/api";
		} catch (Exception ex) {
			System.err.println("Exception: " + ex.getMessage());
		}
	}

	@Before
	public void testSetup() {
		paramsMap.clear();
		headersMap.clear();
	}

	@Test
	public void getUsers() {
		paramsMap.put("page", "1");
		paramsMap.put("per_page", "2");
		headersMap.put("content-type", "application/json");
		headersMap.put("token", "qweasdfq2323asd");
		Response resp = given()
//                .header("content-type", "application/json")
//                .header("token", "qwer1341k134123")
			.headers(headersMap)
			.params(paramsMap)
			.when()
//                .param("page", "2")
//                .param("per_page", 5)
			.get("/users");
		System.out.println(resp.body().prettyPrint());
		assertEquals(resp.getBody().jsonPath().get("page").toString(), "1");
		List<String> users = resp.getBody().jsonPath().getList("data.first_name");
		users.stream()
			.forEach(n -> System.out.printf("User: %s\n", n));
		List<User> usersObj = resp.getBody().jsonPath().getList("data", User.class);
		usersObj
			.stream()
			.forEach(System.out::println);
	}

	@Test
	public void createUser() {
		NewUser newUser = new NewUser();
		newUser.setName("Johan");
		newUser.setJob("Tester");
		Response resp = given()
			.header("content-type", "application/json")
			.body(newUser)
			.when()
			.post("/users");
		System.out.println(resp.body().prettyPrint());
		assertEquals(201, resp.getStatusCode());
		assertEquals(newUser.getName(), resp.getBody().jsonPath().get("name"));
	}

	@Test
	public void deleteUser() {
		Response resp = given()
			.header("content-type", "application/json")
			.when()
			.delete("/users/3");
		assertEquals(204, resp.getStatusCode());
	}

	@Test
	public void getUser() {
		headersMap.put("content-type", "application/json");
		headersMap.put("token", "qweasdfq2323asd");
		Response resp = given()
			.headers(headersMap)
			.params(paramsMap)
			.when()
			.get("/users/3");
		System.out.println(resp.body().prettyPrint());
		User u = new User();
		String tmpStr = resp.getBody().jsonPath().get("data.first_name");
		u.setFirst_name(tmpStr);
		tmpStr = resp.getBody().jsonPath().get("data.last_name");
		u.setLast_name(tmpStr);
		tmpStr = resp.getBody().jsonPath().get("data.avatar");
		u.setAvatar(tmpStr);
		int id = resp.getBody().jsonPath().get("data.id");
		u.setId(id);
		System.out.println(">>>" + u);
		assertEquals(u.getLast_name(), "Wong");
	}
}
